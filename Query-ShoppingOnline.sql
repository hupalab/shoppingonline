
IF NOT EXISTS (
   SELECT name
   FROM sys.databases
   WHERE name = N'ShoppingOnline'
)
/*drop database
USE master;
GO

ALTER DATABASE ShoppingOnline SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
GO

DROP DATABASE ShoppingOnline;  
 */


CREATE DATABASE ShoppingOnline

GO
alter database [ShoppingOnline] set query_store=on

/*
GO
USE [ShoppingOnline]
IF OBJECT_ID('Users', 'U') IS NOT NULL
DROP TABLE Users

Go
IF OBJECT_ID('Category', 'U') IS NOT NULL
DROP TABLE Category
GO
*/
-- Create the table in the specified schema
CREATE TABLE dbo.Users
(
   Id BIGINT IDENTITY(1, 1) NOT NULL,
   UserName nvarchar(100), 
   Email nvarchar(50),
   Password nvarchar(100),
   Location nvarchar(100),
   Lat float,
   Lon float,
   PhoneNumber nvarchar(14),
   CreatedOn datetime,
   IsAdmin int, 
   Avatar nvarchar(100),
   primary key(Id)
	
);
GO

CREATE TABLE dbo.UserDetail
(
   UserId BIGINT NOT NULL,
   FirtName nvarchar(50),
   LastName nvarchar(50),
   Address1 nvarchar(200),
   Address2 nvarchar (200),
   Country nvarchar (100),
   States nvarchar(100),
   Zip nvarchar (10),
   primary key(UserId),
   foreign key (UserId) references Users(Id)
	
);

GO

CREATE TABLE dbo.Cate
(
   Id BIGINT IDENTITY(1,1) NOT NULL,
   CategoryName nvarchar(50),
   primary key(Id),

);

GO

CREATE TABLE dbo.Product
(
   Id BIGINT IDENTITY(1,1) NOT NULL,
   ProductName nvarchar(50),
   Price float,
   ProductDescription nvarchar(400),
   ImageUrl nvarchar (200),
   Rate int,
   Favourite int,
   CateId bigint not null,
   primary key(Id),
   foreign key (CateId) references Cate(Id),
	
);

GO

create table dbo.UsersProduct
(
	UserId bigint not null,
	ProductId bigint not null,
	Quatity int,
	PayMent nvarchar(100),
	TotalAmount float,

	primary key (UserId, ProductId),
	foreign key (UserId) references Users(Id),
	foreign key (ProductId) references Product(Id),
)
GO

create table dbo.Customer
(
	Id bigint not null,
	Name nvarchar(100),
	Email nvarchar(100),
	State nVarchar(100),

	primary key (Id),
)

GO

create table dbo.Orders
(
	Id bigint not null,
	CustomerId bigint not null,
	OrderTotal decimal,
	Placed datetime,
	Completed datetime,

	primary key (Id),
	foreign key (CustomerId) references Customer(Id),
)
