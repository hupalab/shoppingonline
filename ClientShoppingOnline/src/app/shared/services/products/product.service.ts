import { Injectable } from '@angular/core';
import { AuthService } from '../auth-admin.service';
import { Product } from '../../models/product';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap, map, retry } from 'rxjs/operators';
import { ProductAdd } from 'src/app/layouts/product/add-product/add-product.component';
import { stringify } from '@angular/compiler/src/util';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

	// initial array
  	products: Observable <Product[]>;
	product: Observable <Product>;

	// // favouriteProducts
	favouriteProducts: FavouriteProduct;
	cartProducts: FavouriteProduct;

	// NavbarCounts
	navbarCartCount = 0;
  	navbarFavProdCount = 0;
  
	// url api
	url = 'http://localhost:5000/api';

	// url mockup test
	 private REST_API_SERVER = "http://localhost:3000";

	 //get token after login
	 token = localStorage.getItem("jwt");
	
	 httpOptions = {
		headers: new HttpHeaders({
		  'Content-Type':  'application/json',
		  'Authorization': this.token
		})
	  };


	constructor(
		private authService: AuthService,
		private http: HttpClient
	) { }


	private  handleError(error: HttpErrorResponse) {
		let errorMessage = 'Unknown error!';
		if (error.error instanceof ErrorEvent) {
		  // Client-side errors
		  errorMessage = `Error: ${error.error.message}`;
		} else {
		  // Server-side errors
		  errorMessage = `Not allow delete this product- Error Code: ${error.status}\nMessage: ${error.message}`;

		}
		window.alert(errorMessage);
		return throwError(errorMessage);
	  }
	
	  
	getProductsPagi(pageIndex: number, pageSize: number){
		return this.http.get<Product[]>(this.url + '/products' + '/' + pageIndex + '/' + pageSize)
		.pipe(retry(3), catchError(this.handleError));
	}

	getProducts(){
		return this.http.get<Product[]>(this.url + '/products')
		.pipe(retry(3), catchError(this.handleError));
	}

	showProduct() {
		this.getProducts()
		.subscribe((res: any) => this.products = res);
		console.log(this.products);
	  }


	createProduct(p: Product) {
		// JSON.stringify(p)
		// console.log("add product:" + JSON.stringify(p));
		var httpOptions = {
			headers: new HttpHeaders({
			  'Content-Type':  'application/json',
			  'Authorization': this.token
			})
		  };

			/* //body send
			{
				"ProductName": "bun bo ngon lam",
				"CateId": 2,
				"Price": 12,
				"ProductDescription": "bun bo sdf asdf",
				"ImageUrl": "bun bsdf ",
				"Rate": 1,
				"Favourite": true
			}
			*/

		var body = {  
			productName: p.ProductName,
			price: p.Price,
			productDescription: p.ProductDescription,
			imageUrl: p.ImageUrl,
			rate: p.Rate,
			cateId: p.CateId,
			favourite: p.Favourite
		} 
		console.log(body);
		return this.http.post(this.url + "/products",body , httpOptions);
		
	}

	deleteProduct(p: Product) {
		var httpOptions = {
			headers: new HttpHeaders({
			  'Content-Type':  'application/json',
			  'Authorization': this.token
			})
		  };
		  //can set ID url brower
		 // const params = new HttpParams().set('ID', p.Id);

		 var id = p.Id;
		const apiurl = `${this.url}/products/${+id}`;
		// console.log(apiurl);
		return this.http.delete<Product>(apiurl, httpOptions).pipe(retry(3), catchError(this.handleError));
		//call delete web service
		// console.log("delete product");
		// this.products.remove(id);
	}

	updateProduct(p: Product) {

		// const params = new HttpParams().set('id', p.Id.toString());  
		var httpOptions = {
			headers: new HttpHeaders({
			  'Content-Type':  'application/json',
			  'Authorization': this.token
			})
		  };

		  var body = {  
			id: p.Id,
			productName: p.ProductName,
			price: p.Price,
			productDescription: p.ProductDescription,
			imageUrl: p.ImageUrl,
			rate: p.Rate,
			cateId: p.CateId,
			favourite: p.Favourite
		} 

		// JSON.stringify(data)
		console.log(body);
		return this.http.put<Product>(this.url + "/products/"+body.id, body, httpOptions);
		
	}

	getProductById(id: number) {
		const apiurl = `${this.url}/products/${id}`;
		// console.log(apiurl);
		return this.http.get(apiurl);
	}

	getcateId(data) {
		const apiurl = `${this.url}/cates/${data.cateId}`;
		// console.log(apiurl);
		return this.http.get(apiurl);
	}

	getCate() {
		const apiurl = `${this.url}/cates`;
		// console.log(apiurl);
		return this.http.get(apiurl);
	}

	addToCart(data: Product): void {
		let a: Product[];

		a = JSON.parse(localStorage.getItem('avct_item')) || [];

		a.push(data);
		setTimeout(() => {
			localStorage.setItem('avct_item', JSON.stringify(a));
			this.calculateLocalCartProdCounts();
		}, 500);
	}

	calculateLocalCartProdCounts() {
		this.navbarCartCount = this.getLocalCartProducts().length;
	}

		// Fetching Locat CartsProducts
	getLocalCartProducts(): Product[] {
		const products: Product[] = JSON.parse(localStorage.getItem('avct_item')) || [];

		return products;
	}

	// Removing cart from local
	removeLocalCartProduct(product: Product) {
		const products: Product[] = JSON.parse(localStorage.getItem('avct_item'));

		for (let i = 0; i < products.length; i++) {
			if (products[i].Id === product.Id) {
				products.splice(i, 1);
				break;
			}
		}
		// ReAdding the products after remove update 
		localStorage.setItem('avct_item', JSON.stringify(products));

		this.calculateLocalCartProdCounts();
	}

	
	addFavouriteProduct(data: Product): void {
		let a: Product[];
		a = JSON.parse(localStorage.getItem('avf_item')) || [];
		a.push(data);
		setTimeout(() => {
			localStorage.setItem('avf_item', JSON.stringify(a));
			this.calculateLocalFavProdCounts();
		}, 1500);
	}

	calculateLocalFavProdCounts() {
		this.navbarFavProdCount = this.getLocalFavouriteProducts().length;
	}
	getLocalFavouriteProducts(): Product[] {
		const products: Product[] = JSON.parse(localStorage.getItem('avf_item')) || [];

		return products;
	}

}


export class FavouriteProduct {
	product: Product;
	productId: string;
	userId: string;
}
