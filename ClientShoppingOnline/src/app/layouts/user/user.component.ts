import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth-admin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(router:Router) { }

  ngOnInit() {
  }

}
