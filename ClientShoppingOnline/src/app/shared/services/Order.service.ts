import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Order } from 'src/app/shared/models/Order';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

constructor(private http: HttpClient) { }

// baseUrl: string ='http://localhost:5000/api/order';

orderItem;
Formdata;

getOrder() : Observable<Order> {
  return this.http.get<Order>(environment.apiUrl+"/order");
}

}
