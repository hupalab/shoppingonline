import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

constructor(private jwtHelper: JwtHelperService) { }

isLoggedIn(): boolean {
  //check user logged? true: false
  var token = localStorage.getItem("jwt");

  if (token && !this.jwtHelper.isTokenExpired(token)){
    return true;
  } else {
      return false;
  }
}

isAdmin(): boolean {

   //check user role? true: false // check role admin, author, guess
  var token = localStorage.getItem("jwt");

  if (token && !this.jwtHelper.isTokenExpired(token)){
    return true;
  } else {
      return false;
  }
}

public logOut = () => {
  localStorage.removeItem("jwt");
}

}

