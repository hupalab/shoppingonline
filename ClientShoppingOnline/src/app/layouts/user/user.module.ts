import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { UserRoutes } from './user.routing';

import { UserComponent } from './user.component';
import { UserAccountComponent } from './user-account/user-account.component';
import { UserProfileComponent } from './user-profile/user-profile.component';

@NgModule({
  imports: [
    CommonModule, SharedModule, RouterModule.forChild(UserRoutes)
  ],
  declarations: [UserComponent, UserAccountComponent, UserProfileComponent],
  providers: [],
})
export class UserModule { }
