import { Component, OnInit, Optional,Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { OrderService } from 'src/app/shared/services/Order.service';
import { FoodService } from 'src/app/shared/services/Food.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  fromPage: any;
  submited = false;
  formFoodItem: FormGroup;

  foodlist: {}; 
  // public formFoodItem: Food; 
 
  constructor(public dialogRef: MatDialogRef<OrderComponent>, 
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any={},
    private orderService: OrderService,
    private food: FoodService,
    private fb: FormBuilder
    ) { 
      if(this.data.pageValue == "undefined"){
        console.log("data empty");
      }
      this.fromPage = data.pageValue;

      this.formFoodItem = this.fb.group({
        FoodId: [1],
        Name: ["stirng"],
        Price: [0],
        Quantity: [0],
        Total: [0],
      })
      // debugger;
  }

  ngOnInit() {
   this.food.getFood().subscribe(data => {this.foodlist = data;  },  err => console.log('HTTP Error', err), );
    this.formFoodItem;
  }



    get diagnostic() { return JSON.stringify(this.formFoodItem); }

  submitFoodItem(formFoodItem) {
    this.submited = true

    if(this.data.pageValue=="undefined") {
      console.log("error");
    }
    this.dialogRef.close({event:'Submit',data:formFoodItem.value}); 

    // this.dialogRef.afterClosed().subscribe(result => {
    //   console.log(`Dialog result: ${result.data}`); // form value submit
    // });
    // console.log(formDialog);
  }

  closeDialog(){ 
    this.dialogRef.close({event:'close'}); 
  }

    //only input number
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

}
