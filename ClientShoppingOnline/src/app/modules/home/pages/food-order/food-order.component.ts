import { Component, OnInit, HostListener } from '@angular/core';
import { MatDialogConfig, MatDialog, MatDialogRef } from '@angular/material';
import { OrderComponent } from './order/order.component';
import { OrderService } from 'src/app/shared/services/Order.service';
import { Order } from 'src/app/shared/models/Order';
import { CustomerService } from 'src/app/shared/services/Customer.service';
import { Customer } from 'src/app/shared/models/Customer';
import { NgForm } from '@angular/forms';
import { FoodService } from 'src/app/shared/services/Food.service';
import { Food } from 'src/app/shared/models/Food';


@Component({
  selector: 'app-food-order',
  templateUrl: './food-order.component.html',
  styleUrls: ['./food-order.component.scss']
})
export class FoodOrderComponent implements OnInit {
  sendValue: string;
  dialogValue: any;
  orders: Order;
  customerList: Customer;
  food: Food;

  constructor(public dialog: MatDialog, 
    private orderService: OrderService,
    private customerService: CustomerService,
    private foodService: FoodService,
  ) { }

  ngOnInit() {
    this.customerService.getCustomer().subscribe( data => this.customerList = data);
    // debugger;
    this.foodService.getFood().subscribe(data => this.food = data);
    this.resetForm();
  }
  resetForm(form?: NgForm) {
    if(form = null) 
    form.resetForm();
    this.orderService.Formdata = {
      orderId: null,
      orderNo: Math.floor(100 + Math.random()* 900).toString(),
      customerId: 0,
      payment: '',
      totalAmount: 0,
      deleteOrderFoodId: ''
    }


  }

  openModal() {
    const dialogRef = this.dialog.open(OrderComponent, {
      width: '75%',
      // backdropClass:'custom-dialog-backdrop-class',
      // panelClass:'custom-dialog-panel-class',
      data: {pageValue: this.sendValue} || {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed',result);
      this.dialogValue = result.data;
    });
    
  }

getOrder() {
  this.orderService.getOrder().subscribe( data => {
      this.orders = data;
     console.log(this.orders);
  });
  
}

}
