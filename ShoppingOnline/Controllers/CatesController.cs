﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShoppingOnline.Models;
using ShoppingOnline.Models.ViewModel;

namespace ShoppingOnline.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CatesController : ControllerBase
    {
        private readonly ShoppingOnlineContext _context;

        public CatesController(ShoppingOnlineContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Cate>>> GetCate()
        {
                return _context.Cate.ToList();
        }

        // GET: api/Cates
    [Route("products")]

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ViewCateProducts>>> GetCatePrducts()
        {
             List<Product> product = _context.Product.ToList();
            List<Cate> cate = _context.Cate.ToList();

            var cateProducts = from p in product
                                join c in cate on p.CateId equals c.Id 
                                select new ViewCateProducts {
                                    Id = c.Id,
                                    CategoryName = c.CategoryName,
                                    ProductId = p.Id,
                                    ProductName = p.ProductName,
                                    ProductDescription = p.ProductDescription,
                                    Price = p.Price,
                                    ImageUrl = p.ImageUrl,
                                    Rate = p.Rate,
                                    Favourite = p.Favourite

                                };

                return cateProducts.ToList();
        }

        // GET: api/Cates/5
        [HttpGet("{id}")]
        // [Authorize(Roles = "administrator")]
        public async Task<ActionResult<Cate>> GetCate(long id)
        {
            var cate = await _context.Cate.FindAsync(id);

            if (cate == null)
            {
                return NotFound();
            }

            return cate;
        }

        // PUT: api/Cates/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        [Authorize(Roles = "administrator")]

        public async Task<IActionResult> PutCate(long id, Cate cate)
        {
            if (id != cate.Id)
            {
                return BadRequest();
            }

            _context.Entry(cate).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CateExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Cates
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        [Authorize(Roles = "administrator")]

        public async Task<ActionResult<Cate>> PostCate(Cate cate)
        {
            _context.Cate.Add(cate);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCate", new { id = cate.Id }, cate);
        }

        // DELETE: api/Cates/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "administrator")]

        public async Task<ActionResult<Cate>> DeleteCate(long id)
        {
            var cate = await _context.Cate.FindAsync(id);
            if (cate == null)
            {
                return NotFound();
            }

            _context.Cate.Remove(cate);
            await _context.SaveChangesAsync();

            return cate;
        }

        private bool CateExists(long id)
        {
            return _context.Cate.Any(e => e.Id == id);
        }
    }
}
