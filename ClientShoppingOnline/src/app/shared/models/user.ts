export class User {
    Id: number;
    UserName: string;
    Email: string;
    Password: string;
    Location: {
      Lat: number;
      Lon: number;
    };
    PhoneNumber: string;
    CreatedOn: string;
    IsAdmin: boolean;
    Avatar: string;
  }
  
  export class UserDetail {
    Id: number;
    FirstName: string;
    LastName: string;
    UserName: string;
    Email: string;
    Address1: string;
    Address2: string;
    Country: string;
    State: string;
    Zip: number;
  }
  