import { Component, OnInit } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';

import { Location, LocationStrategy, PathLocationStrategy, PopStateEvent } from '@angular/common';
// import { PerfectScrollbar } from 'perfect-scrollbar';
import * as $ from "jquery";
import { DataModel } from 'src/app/shared/models/data';
import { HttpClient } from '@angular/common/http';
import { ProductService } from 'src/app/shared/services/products/product.service';
import { format } from 'url';



@Component({
  selector: 'app-administrator',
  templateUrl: './administrator.component.html',
  styleUrls: ['./administrator.component.css']
})
export class AdministratorComponent implements OnInit {

  products: any;
  plabel: string[];
  pdata: number[];
  data: Observable<DataModel>;
  // datap:  {
  //   name: string[],
  //   price: number[]
  // }
  
  constructor(private http: HttpClient,
    private ps: ProductService
    ) { 
    this.ps.getProductsPagi(1,9).subscribe(res => {
        // console.log(res['page']['data']);
        this.products = res['page']['data'];

        // get only price 
        this.pdata = this.products.map(p => p.price);

        // need unique productname => group productName and sum price.
        // this.plabel = this.products.map(p => p.productName);  

        const formattedProducts = this.products.reduce((r, e) => {
            // array[0] is productName; array[1] is price
        r.push([e.productName, e.price]);

            return r;
        }, []);
        // console.log(formattedProducts[0][1]);
        // formattedProducts.map(x => console.log(this.data = x));
        // console.log(this.data);

        // this.plabel = formattedProducts.map(x => x[0]);
        // this.pdata = formattedProducts.map(x => x[1]);
        // console.log(this.plabel);
        

        const p = [];
        const chartData = formattedProducts.reduce((r, e) => {
          this.plabel = e[0];
          this.pdata = e[1];
          // console.log(key);

          r.push([this.plabel, this.pdata]);
          return r;
        },[]);

        console.log(chartData);
        // chartData.map(x => this.data = x);
      //  this.datap.name = this.plabel;
 
        //  return chartData;
      });
      
    // this.data = this.http.get<DataModel>('../../../../assets/data.json');
  }

  ngOnInit() {

  }
}
