export class Order {
    constructor(
       public OrderId: number,
       public OrderNo: string,
       public Customer: string,
       public PayMent: string,
       public TotalAmount: number,
       public Item?: object,
    ) { }

}
