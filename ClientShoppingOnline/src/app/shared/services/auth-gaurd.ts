import { CanActivate, Route, ActivatedRoute, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth-admin.service';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthGaurd implements CanActivate {
    constructor (
        private route: ActivatedRoute,
        private router: Router,
        private authService: AuthService
    ) {}

    canActivate(route, state: RouterStateSnapshot) {
        if(this.authService.isLoggedIn()) {
            return true;
        }
        this.router.navigate(["/login"], { queryParams: { returnUrl: state.url} });
        return false;
    }

}
