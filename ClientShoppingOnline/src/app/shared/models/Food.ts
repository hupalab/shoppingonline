export class Food {
    constructor(
       public FoodId: number,
       public Name: string,
       public Quantity: number,
       public Price: number,
       public Total: number,
    ) {}

}
