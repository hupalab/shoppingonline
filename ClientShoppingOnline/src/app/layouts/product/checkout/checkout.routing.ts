import { Routes, RouterModule } from '@angular/router';

import { CheckoutComponent } from './checkout.component';
import { BillingDetailsComponent } from './billing-details/billing-details.component';
import { ShippingDetailsComponent } from './shipping-details/shipping-details.component';
import { CheckoutNavbarComponent } from './checkout-navbar/checkout-navbar.component';
import { AuthGaurd } from 'src/app/shared/services/auth-gaurd';
import { ProductsComponent } from './products/products.component';
import { CheckoutCartComponent } from './checkout-cart/checkout-cart.component';
import { CheckoutPaymentComponent } from './checkout-payment/checkout-payment.component';


export const CheckoutRoutes: Routes = [
  { 
    path: 'products/checkouts',
    canActivate: [ AuthGaurd ], 
    children: [
      {
        path: '',
        component: CheckoutComponent,
        // outlet: 'checkOutlet'
      },
      {
        path: 'checkout-cart',
        component: CheckoutCartComponent,
        // outlet: 'checkOutlet'
      },
      {
        path: 'billing-details',
        component: BillingDetailsComponent,
        // outlet: 'checkOutlet'
      },
      {
        path: 'shipping-details',
        component: ShippingDetailsComponent,
        // outlet: 'checkOutlet'
      },
      {
        path: 'checkout-payment',
        component: CheckoutPaymentComponent,
        // outlet: 'checkOutlet'
      }

    ]
   },
];

