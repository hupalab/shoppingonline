import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User, UserDetail } from 'src/app/shared/models/user';
import { Product } from 'src/app/shared/models/product';
import { AuthService } from 'src/app/shared/services/auth-admin.service';

@Component({
  selector: 'app-shipping-details',
  templateUrl: './shipping-details.component.html',
  styleUrls: ['./shipping-details.component.css']
})
export class ShippingDetailsComponent implements OnInit {
  userDetails: User;
  userDetail: UserDetail;
  products: Product[];

  constructor(
    authService: AuthService,
    private router: Router,
    ) {
    /* Hiding products Element */
		// document.getElementById('productsTab').style.display = 'none';
		// document.getElementById('shippingTab').style.display = 'block';
		// document.getElementById('billingTab').style.display = 'none';
    // document.getElementById('resultTab').style.display = 'none';
    
    this.userDetail = new UserDetail();
    this.userDetails = new User();
   }

  ngOnInit() {
 
  }

  
	updateUserDetails(form: NgForm) {
		const data = form.value;
    console.log(data);


		this.router.navigate([ 'products/checkouts/billing-details']);
	}

}
