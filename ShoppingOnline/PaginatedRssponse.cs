  
using System.Collections.Generic;
using System.Linq;


namespace ShoppingOnline.Controllers
{
    public class PaginatedResponse<T>
    {
        /*
            Data: get each page with parameter
            Total: count items in table.
            ex: pageIndex = 5 and len = 10 <=> (current_page - 1) * limit 
            pass parameter on browser addrss oredes/2/3 => skip(3).take(3) get 3 items next
        */

        public PaginatedResponse(IEnumerable<T> data, int i, int len) 
        {
            if( i > 0 ) {
                Data = data.Skip((i - 1) * len).Take(len).ToList();
                Total = data.Count();
            }

        }

        public int Total{get; set;}
        public IEnumerable<T> Data {get; set;}

    }
}