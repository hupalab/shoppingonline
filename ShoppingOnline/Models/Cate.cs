﻿using System;
using System.Collections.Generic;

namespace ShoppingOnline.Models
{
    public partial class Cate
    {
        public Cate()
        {
            Product = new HashSet<Product>();
        }

        public long Id { get; set; }
        public string CategoryName { get; set; }

        public virtual ICollection<Product> Product { get; set; }
    }
}
