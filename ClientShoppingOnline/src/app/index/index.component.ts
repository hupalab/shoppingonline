import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../shared/services/Customer.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  customer: any; 
  constructor(private getCust: CustomerService) { }

  ngOnInit() {
  }

  getCustomer() {
    this.getCust.getCustomer().subscribe(res => this.customer = res );
  }

  logOut() {
    
    localStorage.removeItem("jwt");
 }
 
}
