﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShoppingOnline.Models;

namespace ShoppingOnline.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly ShoppingOnlineContext _context;

        public OrdersController(ShoppingOnlineContext context)
        {
            _context = context;
        }

        // GET: api/Orders
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Orders>>> GetOrders()
        {
            return await _context.Orders.ToListAsync();
        }

       [HttpGet("{pageIndex:int}/{pageSize:int}")]
        public IActionResult Get(int pageIndex, int pageSize) {
            var data = _context.Orders.OrderByDescending(c => c.Placed);;

            var page = new PaginatedResponse<Orders>(data, pageIndex, pageSize); // Data skip and Total pages

            var totalCount = data.Count();
            var totalPages = Math.Ceiling( (double)totalCount / pageSize );

            var response = new {
                Page = page,
                TotalPages = totalPages
            };

            return Ok(response);
        }

        [HttpGet("bystate")]
        public IActionResult byState() {
            /*
                Form table Orders(Id, CustomerId, orderTotal, completed) Join table Customer(Id, Name, Email, State)
                GroupBy with column State of table Customer (State become "Key")
                Select for get data: State and OrderTotal of table Orders
                Return result query.
            */
            var orders = _context.Orders.Include(o=>o.Customer).ToList();
            
            var result = orders
                        .GroupBy(r => r.Customer.State)
                        .ToList()
                        .Select(r => new {
                            State = r.Key,
                            Total = r.Sum(x => x.OrderTotal)
                        }).ToList();
            return Ok(result);
        }

        [HttpGet("bycustomer/{n}")]
        public IActionResult ByCustomer(int n)
        {
            // enumerated group due to ef error
            // https://github.com/aspnet/EntityFrameworkCore/issues/9551

            var orders = _context.Orders.Include(o => o.Customer).ToList();
            var groupedResult = orders
                .GroupBy(r => r.Customer.Id)
                .ToList()
                .Select(grp => new
                {
                    Name = _context.Customer.Find(grp.Key).Name,
                    Total = grp.Sum(x => x.OrderTotal)
                }).OrderByDescending(r => r.Total)
                .Take(n)
                .ToList();

            return Ok(groupedResult);
        }

        // GET: api/Orders/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Orders>> GetOrders(long id)
        {
            var orders = await _context.Orders.FindAsync(id);

            if (orders == null)
            {
                return NotFound();
            }

            return orders;
        }

        // PUT: api/Orders/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOrders(long id, Orders orders)
        {
            if (id != orders.Id)
            {
                return BadRequest();
            }

            _context.Entry(orders).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrdersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Orders
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Orders>> PostOrders(Orders orders)
        {
            _context.Orders.Add(orders);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (OrdersExists(orders.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetOrders", new { id = orders.Id }, orders);
        }

        // DELETE: api/Orders/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Orders>> DeleteOrders(long id)
        {
            var orders = await _context.Orders.FindAsync(id);
            if (orders == null)
            {
                return NotFound();
            }

            _context.Orders.Remove(orders);
            await _context.SaveChangesAsync();

            return orders;
        }

        private bool OrdersExists(long id)
        {
            return _context.Orders.Any(e => e.Id == id);
        }
    }
}
