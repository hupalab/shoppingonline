import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product.component';
import { RouterModule } from '@angular/router';
import { ProductRoutes } from './product.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { CheckoutModule } from './checkout/checkout.module';
import { BestProductComponent } from './best-product/best-product.component';
import { ProductListComponent } from './product-list/product-list.component';
import { AddProductComponent } from './add-product/add-product.component';
import { FavouriteProductComponent } from './favourite-product/favourite-product.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { CartProductsComponent } from './cart-products/cart-products.component';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule, MatButtonModule, MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { CartCalculatorComponent } from './cart-calculator/cart-calculator.component';
import { ComponentsModule } from 'src/app/shared/components/components.module';


@NgModule({
  imports: [
    CommonModule, 
    // ToasterModule,
    FormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule, 
    MatDialogModule,

    RouterModule.forChild(ProductRoutes), 
    SharedModule, 
    CheckoutModule,
    ComponentsModule
  ],
  declarations: [
    ProductComponent,
    BestProductComponent,
    ProductListComponent,
    AddProductComponent,
    FavouriteProductComponent,
    ProductDetailComponent,
    CartProductsComponent,
    CartCalculatorComponent
  ],
  exports: [BestProductComponent],
  entryComponents: [AddProductComponent],
  providers: [
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}
  ],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class ProductModule { }
