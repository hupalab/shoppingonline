﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingOnline.Models.ViewModel
{
    public class ViewUser
    {

        public long Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Location { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? IsAdmin { get; set; }
        public string Avatar { get; set; }
    }
}
