import { Routes, RouterModule } from '@angular/router';
import { UserAccountComponent } from './user-account/user-account.component';
import { AdminGaurd } from 'src/app/shared/services/admin-gaurd';
import { AuthGaurd } from 'src/app/shared/services/auth-gaurd';
import { UserComponent } from './user.component';
import { UserProfileComponent } from './user-profile/user-profile.component';

export const UserRoutes: Routes = [
  { 
    path: 'users',
    component: UserComponent,
    canActivate: [ AdminGaurd ],
    children: [
      {
        path: '',
        component: UserAccountComponent,
        outlet: 'profileOutlet'
      },
      {
        path: 'user-profile',
        component: UserProfileComponent,
        outlet: 'profileOutlet',
      }

    ]
   },
];

