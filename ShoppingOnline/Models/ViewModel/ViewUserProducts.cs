using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingOnline.Models.ViewModel
{
    public class ViewUserProducts
    {

        public long UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string ProductName { get; set; }

        public double? Price { get; set; }

        public int? Quatity { get; set; }

        public string PayMent { get; set; }

        public double? TotalAmount { get; set; }
         public string ImageUrl { get; set; }
        public int? Rate { get; set; }
        public int? Favourite { get; set; }

        [Display(Name = "Day create")]
        public DateTime? CreatedOn { get; set; }
        public int? IsAdmin { get; set; }
        public string Avatar { get; set; }


    }
}
