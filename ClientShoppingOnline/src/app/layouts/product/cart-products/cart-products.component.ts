import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/shared/models/product';
import { ProductService } from 'src/app/shared/services/products/product.service';

@Component({
  selector: 'app-cart-products',
  templateUrl: './cart-products.component.html',
  styleUrls: ['./cart-products.component.css']
})
export class CartProductsComponent implements OnInit {

  cartProducts: Product[];
	showDataNotFound = true;

	// Not Found Message
	messageTitle = 'No Products Found in Cart';
	messageDescription = 'Please, Add Products to Cart';

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.getCartProduct();
  }

  getCartProduct() {
		this.cartProducts = this.productService.getLocalCartProducts();
  }
  
  removeCartProduct(product: Product) {
    if(confirm("Are you sure to delete ")) {
      console.log("Implement delete functionality here");
    }
		this.productService.removeLocalCartProduct(product);

		// Recalling
		this.getCartProduct();
	}
}
