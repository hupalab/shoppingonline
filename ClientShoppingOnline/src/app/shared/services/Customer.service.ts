import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Customer } from 'src/app/shared/models/Customer';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  customers: Customer;
constructor(private http: HttpClient ) {
  let token = localStorage.getItem("jwt");
 }

getCustomer() : Observable<Customer> {

  return this.http.get<Customer>(environment.apiUrl+"/customers");
}

}
