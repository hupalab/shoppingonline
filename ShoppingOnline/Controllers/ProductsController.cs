﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShoppingOnline.Models;
using ShoppingOnline.Models.ViewModel;

namespace ShoppingOnline.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    
    public class ProductsController : ControllerBase
    {
        private readonly ShoppingOnlineContext _context;

        public ProductsController(ShoppingOnlineContext context)
        {
            _context = context;
        }

        // GET: api/Products => get products when user role admin
        
                // GET: api/Orders
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Product>>> GetProducts()
        {
            return await _context.Product.ToListAsync();
        }


     [HttpGet("sort/{sortProduct}/{filter}/{pageIndex:int}/{pageSize:int}")]
     //test: http://localhost:5000/api/products/sort/name_desc/a/1/4
         public IActionResult GetProductsSort(string sortProduct, string filter, int pageIndex, int pageSize)
        {
        //     var sortP = "";
        //    sortP = String.IsNullOrEmpty(sortProduct)? "name_desc": "";
        //    sortP = sortProduct == "Price"? "price_desc": "";


            var products = from p in _context.Product select p;

            // filter Product Name
            if( !String.IsNullOrEmpty(filter))
            {
                products = products.Where( p => p.ProductName.Contains(filter) );
            }

            // Sort product following ProductName and Price
            switch (sortProduct)
            {
                case "name_desc": 
                    products = products.OrderByDescending(p => p.ProductName);
                    break;
                case "price":
                    products = products.OrderBy(p => p.Price);
                    break;
                case "price_desc":
                    products = products.OrderByDescending(p => p.Price);
                    break;
                 
                default:
                    products = products.OrderBy(p => p.ProductName);
                    break;
            }

            var page = new PaginatedResponse<Product>(products, pageIndex, pageSize);

            var totalCount = products.Count();
            var totalPages = Math.Ceiling( (double)totalCount / pageSize );

            var response = new {
                Page = page,
                TotalPages = totalPages
            };

            return Ok(response);
            // return Ok( await products.ToListAsync() );
        }

        [HttpGet("{pageIndex:int}/{pageSize:int}")]
        public IActionResult Get(int pageIndex, int pageSize) {

            var data = _context.Product.OrderBy(p => p.Id);

            var page = new PaginatedResponse<Product>(data, pageIndex, pageSize);

            var totalCount = data.Count();
            var totalPages = Math.Ceiling( (double)totalCount / pageSize );

            var response = new {
                Page = page,
                TotalPages = totalPages
            };

            return Ok(response);
        }

        [HttpGet("cate")] //[HttpGet("login")]
        public async Task<ActionResult<IEnumerable<ViewProducts>>> GetCateProduct(){
            
            List<Product> product = _context.Product.ToList();
            List<Cate> cate = _context.Cate.ToList();

             var products = from p in product
                                join c in cate on p.CateId equals c.Id 
                                select new ViewProducts {
                                    Id = p.Id,
                                    ProductName = p.ProductName,
                                    ProductDescription = p.ProductDescription,
                                    Price = p.Price,
                                    ImageUrl = p.ImageUrl,
                                    Rate = p.Rate,
                                    Favourite = p.Favourite,
                                    CategoryName = c.CategoryName

                                };

            return products.ToList();
        
        }



        [HttpGet("cate/{n}")]
        public IActionResult ByState(int n)
        {

            var products = _context.Product.Include(o => o.Cate).ToList();
            var groupedResult = products
                .GroupBy(r => r.Cate.Id)
                .ToList()
                .Select(grp => new
                {
                    Name = _context.Cate.Find(grp.Key).CategoryName,
                    Total = grp.Sum(x => x.Price)
                }).OrderByDescending(r => r.Total)
                .Take(n)
                .ToList();

            return Ok(groupedResult);
        }

        // GET: api/Products/5
        [HttpGet("{id}")]
        // [Authorize(Roles = "administrator")]

        public async Task<ActionResult<Product>> GetProduct(long id)
        {
            var product = await _context.Product.FindAsync(id);

            if (product == null)
            {
                return NotFound();
            }
        
            return product;
        }

        // PUT: api/Products/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        [Authorize(Roles = "administrator")]
        public async Task<IActionResult> PutProduct(long id, Product product)
        {
            if (id != product.Id)
            {
                return BadRequest();
            }

            _context.Entry(product).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Products
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        [Authorize(Roles = "administrator")]

        public async Task<ActionResult<Product>> PostProduct(Product product)
        {
            _context.Product.Add(product);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProduct", new { id = product.Id }, product);
        }

        // DELETE: api/Products/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "administrator")]

        public async Task<ActionResult<Product>> DeleteProduct(long id)
        {
            var product = await _context.Product.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            _context.Product.Remove(product);
            await _context.SaveChangesAsync();

            return product;
        }

        private bool ProductExists(long id)
        {
            return _context.Product.Any(e => e.Id == id);
        }
    }
}
