import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FoodOrderComponent } from "./pages/food-order/food-order.component";
import { FoodItemComponent } from "./pages/food-order/food-item/food-item.component";
import { OrderComponent } from "./pages/food-order/order/order.component";

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
      FoodItemComponent,
      FoodOrderComponent,
      OrderComponent,
    ],
  })
  export class HomeModule { }