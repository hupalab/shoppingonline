import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FoodOrderComponent } from './modules/home/pages/food-order/food-order.component';
import { OrdersComponent } from './modules/home/pages/food-order/orders/orders.component';
import { OrderComponent } from './modules/home/pages/food-order/order/order.component';
import { FoodItemComponent } from './modules/home/pages/food-order/food-item/food-item.component';
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';
import { NoAccessComponent } from './shared/components/no-access/no-access.component';


export const AppRouting: Routes = [
  // {path: 'food-order', component: FoodOrderComponent },
  // { path: 'orders', component: OrdersComponent },
  // { path: 'order', component: OrderComponent },
  // { path: 'food', component: FoodItemComponent },
  // { path: 'food-order',   redirectTo: '/food-order', pathMatch: 'full' },
  
  // { path: '',   redirectTo: '/food-order', pathMatch: 'full' },

  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: './index/index.module#IndexModule',
      },
      {
        path: 'products',
        loadChildren: './layouts/product/product.module#ProductModule'
      },
      {
        path: 'users',
        loadChildren: './layouts/user/user.module#UserModule'
      },
      {
        path: 'dashboard',
        loadChildren: './layouts/dashboard/dashboard.module#DashboardModule'
      }
    ]
  },
  { path:'no-access', component: NoAccessComponent },
  { path: '**', component: PageNotFoundComponent }
  
];


