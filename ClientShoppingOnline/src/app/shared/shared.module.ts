import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { FoodService } from './services/Food.service';
import { OrderService } from './services/Order.service';
import { NoAccessComponent } from './components/no-access/no-access.component';
import { AuthService } from './services/auth-admin.service';
import { AuthGaurd } from './services/auth-gaurd';
import { AdminGaurd } from './services/admin-gaurd';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';


@NgModule({
  imports: [
    CommonModule, 
    FormsModule,
    HttpClientModule,
		RouterModule,
  ],
  declarations: [PageNotFoundComponent, NoAccessComponent],
  exports: [
    
    PageNotFoundComponent
  ],
  providers: [AuthService, AuthGaurd, AdminGaurd]
})
export class SharedModule { }
