﻿using System;
using System.Collections.Generic;

namespace ShoppingOnline.Models
{
    public partial class Server
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool? IsOnline { get; set; }
    }
}
