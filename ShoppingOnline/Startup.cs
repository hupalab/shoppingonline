using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using Microsoft.EntityFrameworkCore;
using ShoppingOnline.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using AutoMapper;
using ShoppingOnline.MappingConfigurations;
using System;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.FileProviders;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace ShoppingOnline
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string secretKey = "keyForSecutiry@123"; 
            var symmetricSecurityKey =  new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));

            // configure for file upload
            services.Configure<FormOptions>(o => {
                o.ValueLengthLimit = int.MaxValue;
                o.MultipartBodyLengthLimit = int.MaxValue;
                o.MemoryBufferThreshold = int.MaxValue;
            });
            //include autoapper
            services.AddAutoMapper(typeof(AutoMaprProfile));
            services.AddControllersWithViews();

            services.AddCors();
            services.AddDbContext<ShoppingOnlineContext>(opts => opts.UseSqlServer(Configuration["ConnectionString:ShoppingOnline"]));

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
             .AddJwtBearer(options =>
             {
                 options.TokenValidationParameters = new TokenValidationParameters
                 {
                     ValidateIssuer = true,
                     ValidateAudience = false,
                     ValidateIssuerSigningKey = true,

                     ValidIssuer = "http://localhost:5000",
                     ValidAudience = "readers",
                     IssuerSigningKey = symmetricSecurityKey,
                 };
             });

            services.AddControllers();
            // services.AddTransient<DataSeed>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors(builder => builder.WithOrigins("http://localhost:4200")
            .AllowAnyHeader()
            .AllowAnyMethod());

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseRouting();

            // user login for use
            app.UseAuthentication();
            
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            // config file upload
            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"Resources")),
                RequestPath = new PathString("/Resources")
            });

            /*
            using (var scope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope() )
            {
                var context = scope.ServiceProvider.GetService<DataSeed>();
                context.SeedData(20);

            }
            */

            //seedData
            // var nCustomers = 20;
            // seeder.SeedData(nCustomers);
        }
    }
}
