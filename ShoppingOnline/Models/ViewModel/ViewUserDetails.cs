using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingOnline.Models.ViewModel
{
    public class ViewUserDetails
    {

        public long Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Location { get; set; }



        public string PhoneNumber { get; set; }
        [Display(Name = "Day create")]
        public DateTime? CreatedOn { get; set; }
        public int? IsAdmin { get; set; }
        public string Avatar { get; set; }

        public string FullName {get; set;}
        public string Address1 { get; set;}
        public string Address2 { get; set;}
        public string Country { get; set;}
        public string States { get; set;}
        public string Zip { get; set;}
 








    }
}
