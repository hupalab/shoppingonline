﻿using System;
using System.Collections.Generic;

namespace ShoppingOnline.Models
{
    public partial class UsersProduct
    {
        public long UserId { get; set; }
        public long ProductId { get; set; }
        public int? Quatity { get; set; }
        public string PayMent { get; set; }
        public double? TotalAmount { get; set; }

        public virtual Product Product { get; set; }
        public virtual Users User { get; set; }
    }
}
