﻿using System;
using System.Collections.Generic;

namespace ShoppingOnline.Models
{
    public partial class Customer
    {
        public Customer()
        {
            Orders = new HashSet<Orders>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string State { get; set; }

        public virtual ICollection<Orders> Orders { get; set; }
    }
}
