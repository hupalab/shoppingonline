export class Product {
    Id: number;
    ProductName: string;
    Price: number;
    ProductDescription: string;
    ImageUrl: string;
    Rate: number;
    Favourite: boolean;
    CateId: number;
}
