import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { UploadComponent } from './upload/upload.component';
import { FormBuilder } from '@angular/forms';
import { PaginationComponent } from './pagination/pagination.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    
  ],
  declarations: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    UploadComponent,
    PaginationComponent
    

  ],
  exports: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    UploadComponent,
    PaginationComponent

  ]
})
export class ComponentsModule { }
