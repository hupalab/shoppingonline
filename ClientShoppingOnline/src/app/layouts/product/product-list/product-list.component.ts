import { Component, OnInit, OnDestroy } from '@angular/core';
import { Product } from 'src/app/shared/models/product';
import { AuthService } from 'src/app/shared/services/auth-admin.service';
import { ProductService } from 'src/app/shared/services/products/product.service';
import { AddProductComponent } from '../add-product/add-product.component';
import { MatDialog } from '@angular/material';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  productList = [];
  editCustomer = false;
  pdata;
	//upload image
  dbPath;
  private url = 'http://localhost:5000/';
  // destroy subscribed => avoid memory leaks
  destroy$: Subject<boolean> = new Subject<boolean>();

	loading = false;
	sendValue: string;
	dialogValue: any;
	FormHeader: { };

	id: number;
    productName: string;
    price: number;
    productDescription: string;
    imageUrl: string;
    rate: number;
    favourite: boolean;
	cateId: number;
	cate: any;

	//for pagination
	total = 0;
	page = 1;
	limit = 5;

	constructor(
		public authService: AuthService,
		private productService: ProductService,
		public dialog: MatDialog,
	) { }

	ngOnInit() {
		this.getProducts();
		this.productService.getCate().subscribe(c => this.cate = c); 	
	}  

	getProducts(): void {
		this.productService.getProductsPagi(this.page, this.limit)
		.subscribe((p) =>{  
			this.productList = p['page']['data']; 
			this.total = p['page'].total;
			this.loading = true;
		
		}),err=>{  
		console.log(err);  
		} ;
	}

	//pagination
	goToPrevious(): void {
		// console.log('Previous Button Clicked!');
		this.page--;
		this.getProducts();
	  }
	
	  goToNext(): void {
		// console.log('Next Button Clicked!');
		this.page++;
		this.getProducts();
	  }
	
	  goToPage(n: number): void {
		this.page = n;
		this.getProducts();
	  }

	checkValue(event: any): void{
		console.log(event);
	 }

	 filterCate(id: number) {
		if (id == 0) {
		  return;
		}else {
		  this.cateId = +id; // convert string to number
		}
	  }
	  public uploadFinished = (event) => {

		this.dbPath = this.url.concat(event.dbPath);
		// this.dbPath = event.dbPath;
		// console.log(this.dbPath);
	
	  }
		
	// get data from Form getDummyObject() when user click button Save save(pf)

	//show Form ShoRegForm(e) for edite: if(e!=null){this.setValuesForEdite(e)} else{this.resetValue()}

	// show form for delete showFormDelete(e): setEdit=true; if(e){!=null}{setValuesDelete(e)}

	// variable formHeader = "Edit" or "Add" or "Delete" => switch(regFrom: ngForm) { case "Add"; this.AddProduct; ..}

	// func save(pf: NgForm) { this.pdata = new Product; this.pdata.name = pf.name }

	// html => button add call ShoRegForm() => setEdit = true; button edit call ShowRegForm(e); delete call showFormDelete(e) and setEdit=fasle


	Save(regForm: NgForm)  
	{  
	  this.getObject(regForm);  
	//   console.log(this.getObject(regForm))
	
	  switch(this.FormHeader)  
	  {  
	  case "Add":  
			 this.createProduct(this.pdata);  
	  break;  
	  case "Edit":  
			this.updateProduct(this.pdata);  
	  break;  
	  case "Delete":  
			this.deleteProduct(this.pdata);  
	  break;  
			 default:  
	  break;  
	
	  }  
	}  

	deleteProduct(pdata: Product) {
		this.productService.deleteProduct(this.pdata).subscribe(res => {
			this.editCustomer = false;
			alert("Deleted successfully");
			this.productService.getProducts().subscribe( res => {
				this.productList = res;
			});
		});

		// throw new Error("Method not implemented.");
	}

	createProduct(pdata: Product) {
		// console.log (this.pdata);
		this.productService.createProduct(this.pdata).subscribe( res => {
			this.productList.push(res);
			alert("data added successfully");
			this.productService.getProducts().subscribe( res => {
				this.productList = res;
			});
			this.editCustomer = false;
		})
		, err => {
			console.log("Error occured" + err);
		}
	}

	updateProduct(p: Product) {
		this.productService.updateProduct(this.pdata).subscribe(res => {
			this.editCustomer = false;
			this.productService.getProducts().subscribe( res => {
				console.log(res);
				this.productList = res;
			});
			alert("Product data update successfully");
		});

	}

	getObject(regForm: NgForm): Product {
		this.pdata = new Product();  

		this.pdata.Id =regForm.value.id;  
		this.pdata.ProductName=regForm.value.productName;  
		this.pdata.Price=regForm.value.price;  
		this.pdata.ProductDescription=regForm.value.productDescription; 
		this.pdata.ImageUrl=this.dbPath;
		this.pdata.Rate=regForm.value.rate;
		this.pdata.Favourite = regForm.value.favourite? 1: 0;
		this.pdata.CateId=regForm.value.cateId;

		return this.pdata;  

		
	}

	ShowProduct(p) {
		// console.log(p);
		this.editCustomer = true;
		if(p != null){
			this.SetValuesForEdit(p); 
		} 
		else {
			this.ResetValues();
		}
	}

	ShowDelProduct(p) {
		this.editCustomer = true; 
		if(p != null) {
			this.setValuesForDel(p);
		}
	} 
	
	setValuesForDel(p: any) {
		this.id = p.id;
		this.productName=p.productName;  
		this.price= p.price;  
		this.productDescription=p.productDescription;  
		this.imageUrl=p.imageUrl;
		this.rate = p.rate;
		this.favourite = p.favourite;
		this.cateId = p.cateId;
		this.FormHeader="Delete" ; 
	}
	// reset form
	ResetValues(){  
		this.id = null;
		this.productName="";  
		this.price= null;  
		this.productDescription="";  
		this.imageUrl="";
		this.rate = null;
		this.favourite = true;
		this.cateId = this.cateId;
		this.FormHeader="Add" ; 
	  }  


	SetValuesForEdit(p: any) {
		this.id = p.id;
		this.productName=p.productName;  
		this.price= p.price;  
		this.productDescription=p.productDescription;  
		this.imageUrl=p.imageUrl;
		this.rate = p.rate;
		this.favourite = p.favourite;
		this.cateId = p.cateId;
		this.FormHeader="Edit" ; 
	}

	// ngOnDestroy() {
	// 	this.destroy$.next(true);
	// 	// Unsubscribe from the subject
	// 	this.destroy$.unsubscribe();
	//   }

	// getAllProducts() {
	// 	// this.spinnerService.show();
	// 	this.loading = true;
	// 	this.productService.getProducts()
	// 	.pipe(takeUntil(this.destroy$))
	// 	.subscribe((data: Product[])=>{
	// 		console.log(data);
	// 		this.productList = data;
	// 		// for (let i = 0; i < data.length; i++) {
	// 		// 	this.productList.push(data[i]);
	// 		// }
	// 	  });  
	// }

	addProduct() {
		const dialogRef = this.dialog.open(AddProductComponent, {
			width: '75%',
			// backdropClass:'custom-dialog-backdrop-class',
			// panelClass:'custom-dialog-panel-class',
			data: {pageValue: this.sendValue} || {}
		  });
			 dialogRef.afterClosed().subscribe(result => {
			// console.log('The dialog was closed', result);
			this.dialogValue = result.data;
			console.log(this.dialogValue);
		  });
	}
	/*
	removeProduct(id: number) {
		if(confirm("Are you sure to delete ")) {
			console.log("Implement delete functionality here");
			this.productService.deleteProduct(id);

			var index;
				for (var i = 0; i < this.productList.length; i++) {
					if(this.productList[i].id == id) {
						index = i;
						break;
					}
				}
				if (index) {
					this.productList.splice(index,1);
				} 
		  }

	}
	*/

	addFavourite(product: Product) {
		console.log('add to Favourite');
		this.productService.addFavouriteProduct(product);
		
	}

	addToCart(product: Product) {
		this.productService.addToCart(product);
		console.log('add to cart');
	}

}
