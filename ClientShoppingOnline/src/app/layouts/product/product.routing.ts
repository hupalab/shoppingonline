import { Routes, RouterModule, RouterOutlet } from '@angular/router';
import { ProductListComponent } from './product-list/product-list.component';
import { FavouriteProductComponent } from './favourite-product/favourite-product.component';
import { CartProductsComponent } from './cart-products/cart-products.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductComponent } from './product.component';
import { AdminGaurd } from 'src/app/shared/services/admin-gaurd';
import { IndexComponent } from '../../index/index.component';
import { AuthGaurd } from 'src/app/shared/services/auth-gaurd';

export const ProductRoutes: Routes = [ 
  { 
    path: 'products',
    // component: ProductComponent,
    // canActivate: [AuthGaurd ],
    children: [
      {
        path: '',
        component: ProductListComponent,
        // outlet: 'productsOutlet'
      },
      {
        path: 'favourite-products',
        component: FavouriteProductComponent,
        // outlet: 'productsOutlet'
      },
      {
        path: 'cart-items',
        component: CartProductsComponent,
        // outlet: 'productsOutlet'
      },
      {
        path: 'checkouts',
        loadChildren: './checkout/checkout.module#CheckoutModule',
        // outlet: 'productsOutlet'
      },
      {
        path: ':id',
        component: ProductDetailComponent,
        // outlet: 'productsOutlet'
      }
    ]
   },
];

