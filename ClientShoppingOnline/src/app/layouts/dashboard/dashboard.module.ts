import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';

import { DashboardRoutes } from './dashboard.routing';
import { AdministratorComponent } from './administrator/administrator.component';
import { FormsModule } from '@angular/forms';
import { MatButtonModule, MatRippleModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatTooltipModule } from '@angular/material';
import { ComponentsModule } from 'src/app/shared/components/components.module';
import { MapComponent } from './map/map.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { BarChartComponent } from './bar-chart/bar-chart.component';
import { ProductModule } from '../product/product.module';
import { ProductsComponent } from '../product/checkout/products/products.component';



@NgModule({
  imports: [
    CommonModule, 
    FormsModule,

    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    ComponentsModule,
    ProductModule,
    
    RouterModule.forChild(DashboardRoutes)
  ],
  declarations: [
    DashboardComponent, 
    AdministratorComponent,
    MapComponent,
    NotificationsComponent,
    BarChartComponent,
  ],
})
export class DashboardModule { }
