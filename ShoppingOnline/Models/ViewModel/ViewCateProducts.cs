using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingOnline.Models.ViewModel
{
    public class ViewCateProducts
    {

        public long Id { get; set; }
        public string CategoryName { get; set; }
        public long ProductId { get; set; }

        public string ProductName { get; set; }
        public string ProductDescription { get; set; }

        public double? Price { get; set; }


         public string ImageUrl { get; set; }
        public int? Rate { get; set; }
        public int? Favourite { get; set; }


    }
}
