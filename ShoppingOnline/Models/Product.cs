﻿using System;
using System.Collections.Generic;

namespace ShoppingOnline.Models
{
    public partial class Product
    {
        public Product()
        {
            UsersProduct = new HashSet<UsersProduct>();
        }

        public long Id { get; set; }
        public string ProductName { get; set; }
        public double? Price { get; set; }
        public string ProductDescription { get; set; }
        public string ImageUrl { get; set; }
        public int? Rate { get; set; }
        public int? Favourite { get; set; }
        public long CateId { get; set; }

        public virtual Cate Cate { get; set; }
        public virtual ICollection<UsersProduct> UsersProduct { get; set; }
    }
}
