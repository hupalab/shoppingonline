﻿using System;
using System.Collections.Generic;

namespace ShoppingOnline.Models
{
    public partial class UserDetail
    {
        public long UserId { get; set; }
        public string FirtName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Country { get; set; }
        public string States { get; set; }
        public string Zip { get; set; }

        public virtual Users User { get; set; }

        public string GetFullName()
        {
            // List<float> list1 = new List<float>();
            //    return list1.Add(this.Lat, this.Lon);
            return $"{this.FirtName} {this.LastName}";
        }
    }
}
