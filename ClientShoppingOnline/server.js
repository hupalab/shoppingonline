var faker = require('faker');

var database = { 
    products: [],
    users: []
};

for (var i = 1; i<= 300; i++) {
  database.products.push({
    id: i,
    productName: faker.commerce.productName(),
    category: faker.commerce.productMaterial(),
    quantity: faker.random.number(),
    rating: faker.random.number(),
    description: faker.lorem.sentences(),
    price: faker.commerce.price(),
    imageUrl: "https://source.unsplash.com/1600x900/?product",
    quantity: faker.random.number(),
    favourite: faker.random.boolean()
  });
}

for (var i = 1; i<= 50; i++) {
    database.users.push({
      id: i,
      userName: faker.internet.userName(),
      email: faker.internet.email(),
      password: faker.internet.password(),
      location: [ {Lat: faker.address.latitude()}, {Lon: faker.address.longitude()} ],
      phoneNumber: faker.phone.phoneNumber(),
      createdOn: faker.date.recent(),
      isAdmin: faker.random.boolean(),
      avatar: "https://source.unsplash.com/160x160/?avatar",
      userdetail: [
        {
            id: i,
            firstName: faker.name.findName(),
            lastName: faker.name.lastName(),
            address1: faker.address.streetAddress(),
            country: faker.address.country(),
            state: faker.address.state(),
            zip: faker.address.zipCode()
        }
    
      ]
    });
  }
console.log(JSON.stringify(database));