import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/shared/models/product';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from 'src/app/shared/services/products/product.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  private sub: any;
  product: Array<Product> = [];
  cateName;
  
  constructor(
    private route: ActivatedRoute,
		private productService: ProductService
  ) {
    // this.product = new Product();
   }

  ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      const id = params['id']; // (+) converts string 'id' to a number
      this.productService.getProductById(id).subscribe((data: Product[]) => {
        this.product = data; 
        this.productService.getcateId(data).subscribe(c => this.cateName = c['categoryName']); //; console.log(c);
      } );
      
      // console.log(this.product);
      // this.productService.getcateId(this.product.CateId).subscribe(data => console.log(data));
      // // this.getProductDetail(id);
		});
  }

  addToCart(product: Product) {
    this.productService.addToCart(product);
    
    console.log(this.productService.navbarCartCount);
	}
}
