﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using ShoppingOnline.Models;

namespace ShoppingOnline.Controllers
{


    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly ShoppingOnlineContext _context;

        public AuthController(ShoppingOnlineContext context)
        {
            _context = context;
        }

        [HttpPost, Route("login")]
        public IActionResult Login([FromBody]Models.Users user)
        {
            if (user == null)
            {
                return BadRequest("Invalid client request");
            } 

            if( user != null)
            {
                // create befor save password insert into table User to need hash then get password to compare.
                //string password = HashPassword(user.Password);
                var tempUser = _context.Users.FirstOrDefault(u => u.UserName == user.UserName && u.Password == user.Password);

                if (tempUser == null)
                {
                    return BadRequest("Username or Password is wrong");
                } else 
                {
                   //security key
                   string secretKey = "keyForSecutiry@123"; 

                   // symmetric security key
                   var symmetricSecretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));

                   // signing credentials
                    var signingCredentials = new SigningCredentials(symmetricSecretKey, SecurityAlgorithms.HmacSha256);

                    // add claim
                    var claims = new List<Claim>();
                    claims.Add(new Claim(ClaimTypes.Role, "administrator"));
                    claims.Add(new Claim(ClaimTypes.Role, "manager"));


                   // create token
                    var tokeOptions = new JwtSecurityToken(
                        issuer: "http://localhost:5000",
                        audience: "reader",
                        expires: DateTime.Now.AddHours(1),
                        signingCredentials: signingCredentials,
                        claims: claims
                    );

                   // return token.
                    var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);
                    return Ok(new { Token = tokenString });
                }
                
            }
           
            else
            {
                return Unauthorized();
            }
        }
    
        private string HashPassword(string password)
        {
            // generate a 128-bit salt using a secure PRNG
            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            // Console.WriteLine($"Salt: {Convert.ToBase64String(salt)}");

            // derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));
            // Console.WriteLine($"Hashed: {hashed}");
            return hashed;
        }
    }
}