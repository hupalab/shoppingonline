import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckoutComponent } from './checkout.component';
import { RouterModule } from '@angular/router';
import { CheckoutRoutes } from './checkout.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { BillingDetailsComponent } from './billing-details/billing-details.component';
import { ShippingDetailsComponent } from './shipping-details/shipping-details.component';
import { CheckoutNavbarComponent } from './checkout-navbar/checkout-navbar.component';
import { ProductsComponent } from './products/products.component';
import { CheckoutCartComponent } from './checkout-cart/checkout-cart.component';
import { CheckoutPaymentComponent } from './checkout-payment/checkout-payment.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule, FormsModule, SharedModule, RouterModule.forChild(CheckoutRoutes), 
    
  ],
  declarations: [
    CheckoutComponent,
    BillingDetailsComponent,
    ShippingDetailsComponent,
    CheckoutNavbarComponent,
    ProductsComponent,
    CheckoutCartComponent,
    CheckoutPaymentComponent
  ],
  exports: [ CheckoutComponent ]


})
export class CheckoutModule { }
