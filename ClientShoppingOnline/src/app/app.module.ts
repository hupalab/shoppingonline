import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRouting } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatDialogModule, MatButtonModule,  MatFormFieldModule, 
  MatInputModule,
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ModalComponent } from './modules/home/components/modal/modal.component';
import { FoodItemComponent } from './modules/home/pages/food-order/food-item/food-item.component';
import { FoodOrderComponent } from './modules/home/pages/food-order/food-order.component';
import { OrderComponent } from './modules/home/pages/food-order/order/order.component';
import { HttpClientModule } from '@angular/common/http';
import { OrdersComponent } from './modules/home/pages/food-order/orders/orders.component';
import {ContentChildrenComp, Tab, Pane } from './shared/tab-pane';

// import { HomeModule } from './modules/home/home.module';
import { FooterComponent } from './index/footer/footer.component';
import { IndexModule } from './index/index.module';
import { RouterModule, ROUTES } from '@angular/router';
import { SharedModule } from './shared/shared.module';
import { UserModule } from './layouts/user/user.module';
import { ProductModule } from './layouts/product/product.module';
import { CheckoutModule } from './layouts/product/checkout/checkout.module';
import { JwtModule } from "@auth0/angular-jwt";
import { AuthService } from './shared/services/auth-admin.service';
import { AdminGaurd } from './shared/services/admin-gaurd';
import { DashboardRoutes } from './layouts/dashboard/dashboard.routing';
import { DashboardModule } from './layouts/dashboard/dashboard.module';
import { ComponentsModule } from './shared/components/components.module';

export function tokenGetter() {
   return localStorage.getItem("jwt");
 }
  

@NgModule({
   declarations: [
      AppComponent,
      ModalComponent,
      

      // FoodItemComponent,
      // FoodOrderComponent,
      // OrderComponent,
      // OrdersComponent,
      // ContentChildrenComp,
      // Tab,
      // Pane,
   ],
   imports: [
      BrowserModule,
      
      FormsModule,
      ReactiveFormsModule,
      BrowserAnimationsModule,
      MatDialogModule,
      MatButtonModule,
      MatFormFieldModule,
      MatInputModule,
      HttpClientModule,

      IndexModule,
      SharedModule,
      UserModule,
      ProductModule,
      CheckoutModule,
      DashboardModule,
      ComponentsModule,
      
      RouterModule.forRoot(AppRouting),
      JwtModule.forRoot({
         config: {
           tokenGetter: tokenGetter,
           whitelistedDomains: ["localhost:5000"],
           blacklistedRoutes: []
         }
       })
   ],
   providers: [AuthService, AdminGaurd],
   bootstrap: [
      AppComponent
   ],
   // entryComponents: [
   //    OrderComponent
   // ]
})
export class AppModule { }
