﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShoppingOnline.Models;
using ShoppingOnline.Models.ViewModel;

namespace ShoppingOnline.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    // [Authorize(Roles = "manager")]
    public class UserProductsController : ControllerBase
    {
        private readonly ShoppingOnlineContext _context;

        public UserProductsController(ShoppingOnlineContext context)
        {
            _context = context;
        }

        // GET: api/UserProducts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ViewUserProducts>>> GetUsersProduct()
        {

            List <Users> user = _context.Users.ToList();
            List <Product>  product = _context.Product.ToList();
            List <UsersProduct> userProduct = _context.UsersProduct.ToList();

            var userProducts = from up in userProduct 
                                join u in user on up.UserId equals u.Id into table1
                                from u in table1.ToList()
                                join p in product on up.ProductId equals p.Id into table2
                                from p in table2.ToList()
                                select new ViewUserProducts {
                                    UserId = u.Id,
                                    UserName= u.UserName,
                                    Email = u.Email,
                                    PhoneNumber = u.PhoneNumber,
                                    ProductName = p.ProductName,
                                    Price = p.Price,
                                    Quatity = up.Quatity,
                                    PayMent = up.PayMent,
                                    TotalAmount = up.TotalAmount,
                                    ImageUrl = p.ImageUrl,
                                    Rate = p.Rate,
                                    Favourite = p.Favourite,
                                    CreatedOn = u.CreatedOn,
                                    IsAdmin = u.IsAdmin,
                                    Avatar = u.Avatar
                                };


 
            return userProducts.ToList();
        }

        // GET: api/UserProducts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UsersProduct>> GetUsersProduct(long id)
        {
            var usersProduct = await _context.UsersProduct.FindAsync(id);

            if (usersProduct == null)
            {
                return NotFound();
            }

            return usersProduct;
        }

        // PUT: api/UserProducts/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUsersProduct(long id, UsersProduct usersProduct)
        {
            if (id != usersProduct.UserId)
            {
                return BadRequest();
            }

            _context.Entry(usersProduct).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsersProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/UserProducts
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<UsersProduct>> PostUsersProduct(UsersProduct usersProduct)
        {
            _context.UsersProduct.Add(usersProduct);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UsersProductExists(usersProduct.UserId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetUsersProduct", new { id = usersProduct.UserId }, usersProduct);
        }

        // DELETE: api/UserProducts/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<UsersProduct>> DeleteUsersProduct(long id)
        {
            var usersProduct = await _context.UsersProduct.FindAsync(id);
            if (usersProduct == null)
            {
                return NotFound();
            }

            _context.UsersProduct.Remove(usersProduct);
            await _context.SaveChangesAsync();

            return usersProduct;
        }

        private bool UsersProductExists(long id)
        {
            return _context.UsersProduct.Any(e => e.UserId == id);
        }
    }
}
