import {Component, ContentChildren, Directive, Input, QueryList} from '@angular/core';

@Directive({selector: 'pane-app'})
export class Pane {
  @Input() id !: string;
}

// =====================
@Component({
  selector: 'tab',
  template: `
    <div class="top-level">Top level panes: {{serializedPanes}}</div>
    <div class="nested">Arbitrary nested panes: {{serializedNestedPanes}}</div>
  `
})
export class Tab {
  @ContentChildren(Pane) topLevelPanes !: QueryList<Pane>;
 
  //select descendants of tag <tab>
  @ContentChildren(Pane, {descendants: true}) arbitraryNestedPanes !: QueryList<Pane>;

    // show id = 3
  get serializedPanes(): string {
    return this.topLevelPanes ? this.topLevelPanes.map(p => p.id).join(', ') : '';
  }

  // create new object and show id child 
  get serializedNestedPanes(): string {
    return this.arbitraryNestedPanes ? this.arbitraryNestedPanes.map(p => p.id).join(', ') : '';
  }
}
// =========================

@Component({
  selector: 'example-app',
  template: `
    <tab>
      <pane-app id="1"></pane-app>
      <pane-app id="2"></pane-app>
      <pane-app id="3" *ngIf="shouldShow">
        <tab>
          <pane-app id="3_1"></pane-app>
          <pane-app id="3_2"></pane-app>
        </tab>
      </pane-app>
    </tab>

    <button (click)="show()">Show 3</button>
  `,
})

export class ContentChildrenComp {
  shouldShow = false;

  show() { this.shouldShow = true; }
}