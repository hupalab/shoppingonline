﻿using System;
using System.Collections.Generic;

namespace ShoppingOnline.Models
{
    public partial class Users
    {
        public Users()
        {
            UsersProduct = new HashSet<UsersProduct>();
        }

        public long Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Location { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? IsAdmin { get; set; }
        public string Avatar { get; set; }

        public string GetLocation()
        {
            // List<float> list1 = new List<float>();
            //    return list1.Add(this.Lat, this.Lon);
            return $"{this.Lat}, {this.Lon}";
        }

        public virtual UserDetail UserDetail { get; set; }
        public virtual ICollection<UsersProduct> UsersProduct { get; set; }
    }
}
