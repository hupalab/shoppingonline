﻿using System;
using System.Collections.Generic;

namespace ShoppingOnline.Models
{
    public partial class Orders
    {
        public long Id { get; set; }
        public long CustomerId { get; set; }
        public decimal? OrderTotal { get; set; }
        public DateTime? Placed { get; set; }
        public DateTime? Completed { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
