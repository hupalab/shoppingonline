import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/shared/models/product';

@Component({
  selector: 'app-checkout-payment',
  templateUrl: './checkout-payment.component.html',
  styleUrls: ['./checkout-payment.component.css']
})
export class CheckoutPaymentComponent implements OnInit {

  products: Product[];
	date: number;
	totalPrice = 0;
	tax = 6.4;

  constructor() { 
    // document.getElementById('productsTab').style.display = 'none';
		// document.getElementById('shippingTab').style.display = 'none';
		// document.getElementById('billingTab').style.display = 'none';
    // document.getElementById('resultTab').style.display = 'block';
    
    this.date = Date.now();
  }

  ngOnInit() {
  }

  downloadReceipt() {
		const data = document.getElementById('receipt');
		console.log(data);


	}
}
