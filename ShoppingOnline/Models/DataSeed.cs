using System;
using System.Collections.Generic;
using System.Linq;
using ShoppingOnline.Models;

namespace ShoppingOnline
{
    public class DataSeed {
        private readonly ShoppingOnlineContext _context;

       public DataSeed (ShoppingOnlineContext context){
            _context = context;
        }

        public void SeedData (int n) 
        {

            if (!_context.Server.Any())
            {
                SeedCustomers(n);
                _context.SaveChanges();
            }
        }

        internal void SeedCustomers(int n)
        {
           var severs = BuildCustomerList(n);

           foreach (var sever in severs)
           {
                _context.Server.Add(sever);               
           }
        }

        internal static List<Server> BuildCustomerList(int n)
        {
            var customers = new List<Server>();

            for (int i = 0; i <= n; i++)
            {   

                customers.Add(new Server 
                {
                    Id = i,
                    Name = "phat",
                    IsOnline = true
                });
            }
            return customers;
            
        }
    }
}