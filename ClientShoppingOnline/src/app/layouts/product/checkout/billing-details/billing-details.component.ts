import { Component, OnInit } from '@angular/core';
import { User, UserDetail } from 'src/app/shared/models/user';
import { Product } from 'src/app/shared/models/product';
import { AuthService } from 'src/app/shared/services/auth-admin.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-billing-details',
  templateUrl: './billing-details.component.html',
  styleUrls: ['./billing-details.component.css']
})
export class BillingDetailsComponent implements OnInit {

  userDetails: User;
	products: Product[];
  userDetail: UserDetail;
  
  constructor(
    private authService: AuthService,
    private router: Router
  ) { 
    // document.getElementById('productsTab').style.display = 'none';
		// document.getElementById('shippingTab').style.display = 'none';
		// document.getElementById('billingTab').style.display = 'block';
		// document.getElementById('resultTab').style.display = 'none';

    this.userDetail = new UserDetail();
    this.userDetails = new User();

  }

  ngOnInit() {
  }
  updateUserDetails(form: NgForm) {
		const data = form.value;
    console.log(data);
    
		this.router.navigate([ 'products/checkouts/checkout-payment']);
	}
}
