﻿using AutoMapper;
using ShoppingOnline.Models;
using ShoppingOnline.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingOnline.MappingConfigurations
{
    public class AutoMaprProfile: Profile
    {
        public AutoMaprProfile()
        {
            // Default mapping when property names are same
            CreateMap<Users, ViewUser>();

            // Mapping when property names are different
            //CreateMap<User, UserViewModel>()
            //    .ForMember(dest =>
            //    dest.FName,
            //    opt => opt.MapFrom(src => src.FirstName))
            //    .ForMember(dest =>
            //    dest.LName,
            //    opt => opt.MapFrom(src => src.LastName));
        }

    }
}
