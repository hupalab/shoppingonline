import { Component, OnInit, Inject, Optional, ViewChild } from '@angular/core';
import { Product } from 'src/app/shared/models/product';
import { ProductService } from 'src/app/shared/services/products/product.service';
import { NgForm, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { parseHostBindings } from '@angular/compiler';



@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  // public response: {dbPath: ''};
   dbPath;
   url = 'http://localhost:5000/';

  //user viewChild to get form for handle get, setValue on form.
  @ViewChild('addProductForm', {static: false}) addForm: NgForm
  data1: any;
  product: Product = new Product() ;
  cate: any; 


	constructor(
    private productService: ProductService,
    public dialogRef: MatDialogRef<AddProductComponent>, 
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any={} 
    ) {
      this.data1 = data.pageValue;
    }

  ngOnInit() {
    // setvalue default form
    // this.product.productName = "phat"; 
    // console.log(this.addForm);
    this.productService.getCate().subscribe(c => this.cate =c);
  }


  filterCate(id: number) {
    if (id == 0) {
      return;
    }else {
      this.product['CateId'] = +id; // convert string to number
    }
  }

  createProduct(addProductForm: NgForm) {
    this.product['Price'] = addProductForm.value['Price'];
    this.product['ImageUrl'] = this.dbPath;
    this.product['Rate'] = 1;
    this.product['CateId']    
    // console.log(this.product);
    // call func from service 
    this.productService.createProduct(this.product).subscribe(res => console.log(res));
    addProductForm.reset();

    // this.closeModal();
  }

  updateProduct(id, addProductForm: NgForm) {
    this.product['Price'] = addProductForm.value['Price'];
    this.product['ImageUrl'] = this.dbPath;
    this.product['Rate'] = 1;
    this.product['CateId']    
    // console.log(this.product);
    // call func from service 
    this.productService.createProduct(this.product).subscribe(res => console.log(res));

    this.productService.showProduct();
    // productForm.reset();
    // this.closeModal();
  }
  
  closeModal() {

    this.dialogRef.close(this.data);
  
  }

  public uploadFinished = (event) => {

    this.dbPath = this.url.concat(event.dbPath);
    // this.dbPath = event.dbPath;
    // console.log(this.dbPath);

  }
  

}

export class ProductAdd {
  ProductName: string;
  Price: number;
  ImageUrl: string;
  ProductDescription: string;
  Rate: number;
  CateId: number;
}