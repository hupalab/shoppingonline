import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { RouterModule } from '@angular/router';
import { IndexRoutes } from './index.routing';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule.forChild(IndexRoutes), SharedModule
  ],
  declarations: [IndexComponent, FooterComponent, LoginComponent, NavbarComponent ],
  exports: [ NavbarComponent, FooterComponent ],
  providers: [],
} )
export class IndexModule { }
