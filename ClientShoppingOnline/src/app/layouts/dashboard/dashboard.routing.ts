import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { AdministratorComponent } from './administrator/administrator.component';
import { AdminGaurd } from 'src/app/shared/services/admin-gaurd';
import { MapComponent } from './map/map.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { ProductsComponent } from '../product/checkout/products/products.component';


export const DashboardRoutes: Routes = [
  {  
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [ AdminGaurd ],
    children: [
      {
        path: '',
        component: AdministratorComponent,
        // outlet: 'dashboardOutlet',
      },
      {
        path: 'products',
        component: ProductsComponent,

      },
      {
        path: 'map',
        component: MapComponent,

      },
      {
        path: 'notification',
        component: NotificationsComponent
      }
    ]
  },
];
